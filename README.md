# TINF17B5

Das begleitende Repository für die Vorlesung *Advanced Software Engineering* an der DHWB Karlsruhe.

## Vorlesungseinheiten

### 01.10.2019

* API-Design
    * Geschichte
    * Definition
    * Vor/Nachteile
    * Qualitätsmerkmale
    * Vorgehen beim Entwurf
        * **Use Cases übersprungen**
    * APIs auf Objektebene
        * bis einschließlich Benennung
        * wird fortgeführt in der nächsten Einheit
  
### 08.10.2019

* API-Design
    * APIs auf Objektebene
        * Typisierung
        * Minimale Sichtbarkeit
        * Hilfsmethoden
        * optionale Rückgabewerte
        * Exceptions 
        * Interfaces / Fluent Interfaces
        * Template-Methoden
        * Callback-Methoden
        * Annotationen
        * Immutability
        * Threadsicherheit
    * Kompatibilität
        * Code-Kompatibilität
        * binäre Kompatibilität
        * funktionale Kompatibilität
        * Verwandschaftsbeziehung
        * Design By Contract
        * Änderungen in der Praxis

### 22.10.2019
* API-Design
    * Web APIs -> RESTful HTTP
        * Definition REST
        * Hypermedia
        * HTTP-Standardmethoden
        * HATEOAS
        * Beispiel Webshop
        * Asynchrone Bearbeitung
        * Fehlerbehandlung
        * URI Design
        * Versionierung
        * Partielle Rückgabe
        * Security
* Namen
    * Einführung / Philosophie
    * Beispiele Primzahlenliste und Log

### 25.02.2020
* Namen
    * Interfaces
        * Eigenschaften
        * Objekt-Interfaces
    * Methoden
        * Builder-Methoden
        * Manipulator-Methoden
        * mit Bool-Rückgabe
    * Allgemein
        * Intention Revealing
        * Keine Verschlüsselung 
        * Keine Abkürzung
        * Verwirrung vermeiden
        * Kontext einbeziehen
        * Domänsprache verwenden
        * Keine coolen Namen
    * Fallbeispiele
        * TOps -> Table
        * LogTime von Glider
* Software-Architektur
    * Einleitung
    * Definition Software-Architektur
    * Was macht Software-Architektur

### 03.03.2020

* Software-Architektur
    * Vorgehen
    * Schichtenmodell
    * Hexagonale Architektur

### 10.03.2020

* Klausurvorbereitung
* Software-Architektur
    * CQRS
    * SOA
    * Monolith
    * Microservices
    * Technische Schulden

## Lars Briem
### Programming Principles (Briem)
* SOLID
  * Single Responsibility
  * Open Closed
  * Liskov Substitution
  * Interface Segregation
  * Dependency Inversion
* Tell, don't ask
* Keep it short and simple
* Single level of abstraction
* GRASP
  * Low Coupling
  * High Cohesion
  * Information Expert
  * Creator
  * Indirection
  * Polymorphism
  * Controller
  * Pure Fabrication
  * Protected Variations
* Don't Repeat Yourself
* You ain't gonna need it
* Conway's Law

### Entwurfsmuster (Briem)
* Einführung
* Einteilung
* Dekorierer
* Erbauer
* Kompositum
* Strategie

## Mirko Dostmann
* Domain Driven Design
* Clean Architecture
